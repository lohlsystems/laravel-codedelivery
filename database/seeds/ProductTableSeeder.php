<?php

use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
         * INSTRUCTIONS:
         * instead of creating product factory here, we will do it
         * on CategoryTableSeeder because after creating a category we relate
         * that category to new fake products.
         * It makes easier to create fake products and fake categories.
         * This approach could be different when we know all categories and all products
         * we need to create. So, when we create a new product we just relate the
         * new product to the category we want.
         */
    }
}
