<?php

namespace CodeDelivery\Http\Controllers;

use CodeDelivery\Http\Requests\AdminCouponRequest;
use CodeDelivery\Repositories\CouponRepository;
use CodeDelivery\Services\CouponService;
use Illuminate\Http\Request;

class CouponsController extends Controller
{
    private $repository;
    private $couponService;

    public function __construct(CouponRepository $repository, CouponService $couponService)
    {
        $this->repository = $repository;
        $this->couponService = $couponService;
    }

    public function index()
    {
        $coupons = $this->repository->paginate();
        $list = $this->couponService->getUsedList();
        return view('admin.coupons.index', compact('coupons', 'list'));
    }

    public function create()
    {
        $list = $this->couponService->getUsedList();
        return view('admin.coupons.create', compact('list'));
    }

    public function store(AdminCouponRequest $request)
    {
        $data = $request->all();
        $this->repository->create($data);
        return redirect()->route('admin.coupons.index');
    }

    public function edit($id)
    {
        $coupon = $this->repository->find($id);
        $list = $this->couponService->getUsedList();
        return view('admin.coupons.edit', compact('coupon', 'list'));
    }

    public function update(AdminCouponRequest $request, $id)
    {
        $data = $request->all();
        $this->repository->update($data, $id);
        return redirect()->route('admin.coupons.index');
    }

}
