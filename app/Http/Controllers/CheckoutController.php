<?php

namespace CodeDelivery\Http\Controllers;

use CodeDelivery\Repositories\OrderRepository;
use CodeDelivery\Repositories\ProductRepository;
use CodeDelivery\Repositories\UserRepository;
use Illuminate\Http\Request;

class CheckoutController extends Controller
{
    private $orderRepository;
    private $userRepository;
    private $productRepository;

    public function __construct
    (
        OrderRepository $orderRepository, UserRepository $userRepository, ProductRepository $productRepository
    ){
        $this->orderRepository = $orderRepository;
        $this->userRepository = $userRepository;
        $this->productRepository = $productRepository;
    }

    public function create()
    {
        $products = $this->productRepository->getList();
        return response()->view('customer.order.create', compact('products'));
    }

    public function store()
    {

    }

}
