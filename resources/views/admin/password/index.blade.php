@extends('app')

@section('content')
    <div class="container">
        <h3>Password Change</h3>

        <br>

        @include('errors._check')

        @if (session('error'))
            <div class="alert alert-danger">
                {{ session('error') }}
            </div>
        @endif

        @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif

        {!! Form::open(['route' => 'password.update', 'method' => 'post']) !!}

        <div class="form-group">
            {!! Form::label('email', 'Email:') !!}
            {!! Form::email('email', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('password', 'New Password:') !!}
            {!! Form::password('password',['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('password_repeat', 'Repeat Password:') !!}
            {!! Form::password('password_repeat',['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::submit('Change Password', ['class' => 'btn btn-primary']) !!}
        </div>

        {!! Form::close() !!}
    </div>
@endsection