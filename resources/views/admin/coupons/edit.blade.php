@extends('app')

@section('content')
    <div class="container">
        <h3>Editando cupom</h3>

        @include('errors._check')

        {!! Form::model($coupon, ['route' => ['admin.coupons.update', $coupon->id], 'method' => 'post']) !!}

        @include('admin.coupons._form')

        <div class="form-group">
            {!! Form::submit('Salvar cupom', ['class' => 'btn btn-primary']) !!}
        </div>

        {!! Form::close() !!}
    </div>
@endsection