@extends('app')

@section('content')
    <div class="container">
        <h3>Pedido</h3>
        <hr>
        <h4>Cliente: {{ $order->client->user->name }}</h4>
        <p><b>Data:</b> {{ $order->created_at }}</p>
        <p><b>Total:</b> R$ {{ $order->total }}</p>
        <br>
        <p><b>DADOS DA ENTREGA</b></p>
        <p><b>Endereço:</b> {{ $order->client->address }}</p>
        <p><b>Cidade:</b> {{ $order->client->city }}</p>
        <p><b>Estado:</b> {{ $order->client->state }}</p>
        <br>

        @include('errors._check')

        {!! Form::model($order, ['route' => ['admin.orders.update', $order->id], 'method' => 'post']) !!}

        <div class="form-group">
            {!! Form::label('status', 'Status:') !!}
            {!! Form::select('status', $status, null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('user_deliveryman_id', 'Entregador:') !!}
            {!! Form::select('user_deliveryman_id', $deliveryman, null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::submit('Salvar pedido', ['class' => 'btn btn-primary']) !!}
        </div>

        {!! Form::close() !!}
    </div>
@endsection