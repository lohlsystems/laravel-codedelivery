<?php

namespace CodeDelivery\Http\Controllers;

use CodeDelivery\Http\Requests\AdminPasswordRequest;
use CodeDelivery\Repositories\UserRepository;

class PasswordController extends Controller
{
    private $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.password.index');
    }

    /**
     * @param AdminPasswordRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(AdminPasswordRequest $request)
    {
        $data = $request->all();

        $password = $data['password'];
        $passwordRepeat = $data['password_repeat'];

        if($password !== $passwordRepeat)
            return redirect()->route('password.edit')->with('error', "Passwords don't match");

        array_pull($data, 'password_repeat');
        $data['password'] = bcrypt($password);
        $result = $this->repository->findByField('email', $data['email']);

        if(!isset($result[0]))
            return redirect()->route('password.edit')->with('error', 'Could not find username');

        $result[0]->update($data);

        return redirect()->route('password.edit')->with('success', 'Password changed successfully');

    }
}
