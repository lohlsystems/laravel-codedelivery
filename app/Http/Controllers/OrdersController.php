<?php

namespace CodeDelivery\Http\Controllers;

use CodeDelivery\Http\Requests\AdminOrderRequest;
use CodeDelivery\Repositories\OrderRepository;
use CodeDelivery\Repositories\UserRepository;
use CodeDelivery\Services\OrderService;

class OrdersController extends Controller
{
    private $repository;
    private $userRepository;
    private $orderService;

    public function __construct(OrderRepository $repository, UserRepository $userRepository, OrderService $orderService)
    {
        $this->repository = $repository;
        $this->userRepository = $userRepository;
        $this->orderService = $orderService;
    }

    public function index()
    {
        $orders = $this->repository->paginate();
        $status = $this->orderService->getStatusList();
        return view('admin.orders.index', compact('orders', 'status'));
    }

    public function edit($id)
    {
        $order = $this->repository->find($id);
        $status = $this->orderService->getStatusList();
        $deliveryman = $this->userRepository->getDeliveryman();
        return view('admin.orders.edit', compact('order', 'status', 'deliveryman'));
    }

    public function update(AdminOrderRequest $request, $id)
    {
        $data = $request->all();
        $this->repository->update($data, $id);
        return redirect()->route('admin.orders.index');
    }

}
