<div class="form-group">
    {!! Form::label('code', 'Código:') !!}
    {!! Form::text('code', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('value', 'Valor:') !!}
    {!! Form::text('value', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('used', 'Utilizado:') !!}
    {!! Form::select('used', $list, null, ['class' => 'form-control']) !!}
</div>