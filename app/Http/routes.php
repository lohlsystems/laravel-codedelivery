<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('token', function () { return csrf_token(); });
Route::get('password-change', 'PasswordController@index')->name('password.edit');
Route::post('password-update', 'PasswordController@update')->name('password.update');


Route::group(['prefix' => 'admin', 'middleware' => 'auth.checkrole','as' => 'admin.'], function (){
    Route::group(['prefix' => 'categories', 'as' => 'categories.'], function (){
        Route::get('', 'CategoriesController@index')->name('index');
        Route::get('create', 'CategoriesController@create')->name('create');
        Route::get('edit/{id}', 'CategoriesController@edit')->name('edit');
        Route::post('store', 'CategoriesController@store')->name('store');
        Route::post('update/{id}', 'CategoriesController@update')->name('update');
    });
    Route::group(['prefix' => 'products', 'as' => 'products.'], function (){
        Route::get('', 'ProductsController@index')->name('index');
        Route::get('create', 'ProductsController@create')->name('create');
        Route::get('edit/{id}', 'ProductsController@edit')->name('edit');
        Route::post('store', 'ProductsController@store')->name('store');
        Route::post('update/{id}', 'ProductsController@update')->name('update');
        Route::get('delete/{id}', 'ProductsController@destroy')->name('delete');
    });
    Route::group(['prefix' => 'clients', 'as' => 'clients.'], function (){
        Route::get('', 'ClientsController@index')->name('index');
        Route::get('create', 'ClientsController@create')->name('create');
        Route::get('edit/{id}', 'ClientsController@edit')->name('edit');
        Route::post('store', 'ClientsController@store')->name('store');
        Route::post('update/{id}', 'ClientsController@update')->name('update');
    });
    Route::group(['prefix' => 'orders', 'as' => 'orders.'], function (){
        Route::get('', 'OrdersController@index')->name('index');
        Route::get('/edit/{id}', 'OrdersController@edit')->name('edit');
        Route::post('/update/{id}', 'OrdersController@update')->name('update');
    });
    Route::group(['prefix' => 'coupons', 'as' => 'coupons.'], function (){
        Route::get('', 'CouponsController@index')->name('index');
        Route::get('create', 'CouponsController@create')->name('create');
        Route::get('edit/{id}', 'CouponsController@edit')->name('edit');
        Route::post('store', 'CouponsController@store')->name('store');
        Route::post('update/{id}', 'CouponsController@update')->name('update');
    });
});

Route::group(['prefix' => 'customer', 'as' => 'customer.'], function (){
    Route::group(['prefix' => 'order', 'as' => 'order.'], function (){


        Route::get('', 'CheckoutController@index')->name('index');
        Route::get('create', 'CheckoutController@create')->name('create');
        Route::get('edit/{id}', 'CheckoutController@edit')->name('edit');
        Route::post('store', 'CheckoutController@store')->name('store');
        Route::post('update/{id}', 'CheckoutController@update')->name('update');
    });
});

