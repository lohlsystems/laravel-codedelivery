# Curso de Laravel

## Conteúdo e Cronograma do Curso

### Semana 01
* Criando base do sistema - OK
* Repositories - OK
* Sistema Administrativo - OK
* Clientes - OK

### Semana 02
* Pedidos - OK
* Checkout - watching
* Autenticação na API com OAuth2 - watching
* Criando API de Client - watching
* Criando API de Deliveryman
* Validações e Serializações

### Semana 03
* Introdução ao Ionic
* Iniciando com Rotas e Navegação
* Autenticação e Cors
* Serviços e AngularJS

### Semana 04
* Área de pedidos
* Cupom e QRCode

### Semana 05
* Menus e Pedidos
* Área do entregador
* Geoposicionamento - Deliveryman
* Geoposicionamento - Client

### Semana 06
* Ionic Push Notification
* Publicando APP
* Extras
* Build de IOS no Laravel com Ionic
* Errata Push Notification


## Configurações

### Mudar o nome da aplicação
```
php artisan app:name CodeDelivery
```

### Criar o diretório das Models
```php
/app/Models
```
### Transferir a model User.php
```php
/app/Models/User.php
```

### Alterar o namespace da model User
```php
namespace Models\CodeDelivery;
```

### /config/auth.php
```php
'model' => Models\CodeDelivery\User::class,
```

### /config/database.php
```php

```

### /.env
```env
APP_ENV=local
APP_DEBUG=true
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_DATABASE=codedelivery_curso
DB_USERNAME=root
DB_PASSWORD=root
```

### Criar o banco de dados
//
 
### Gerar as tabelas
```
php artisan migrate
```

## Forcing config changes
### Laravel
```
php artisan config:clear
```

```
php artisan cache:clear
```

### Composer
```
composer dump-autoload
```

```
composer clear-cache
```

```
composer update
```

```
composer self-update
```

## Desenvolvimento da aplicação

### Criar uma Migration
* "make:migration" Irá criar uma migration  
* "--create" determina o nome da tabela no banco de dados
```
php artisan make:migration create_categories_table --create=categories
```

### Diretório das Migrations
> /database/migrations

### Criar um Model
```
php artisan make:model Category
```

### Factory
> /database/factories/ModelFactory
```php
$factory->define(CodeDelivery\Models\Category::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word,
    ];
});
```

### Criar um Seed
* Serve para gerar uma massa de modelos que serão salvs no banco de dados com o intuito de popular automaticamente com registros uma determinada tabela
* Dados de exemplo para testar o sistema

> /database/seeds/DatabaseSeeder.php

## Modelagem de dados
Quando se trata da modelagem de dados é necessário ter em mente 4 elementos:
* Migration
* Model
* Seed
* Factory

### Migration
* Determina a estrutura do dado 
* Cria as tabelas e relacionamentos no banco de dados
* Permite controlar, de forma centralizada, as modificações da estrutura do banco de dados e aplicar ao banco

### Model
* É a camada que se comunica com a aplicação
* Através da model toda a intervenção do software ocorrerá na Model, porém, não haverá a persistencia das modificações
* A model é responsavel por definir as regras e persistencia e comunicaçao com banco de dados
* Regra de negócios relacionadas ao dado a ser trabalhado também ficam armazenados no código da model
* Qualquer operação que envolva uma Model ou uma ação com banco de dados, a logica para realizar tal ação deve ser 
implementada em métodos na Model e não no corpo do Controller
* Também é importante já criar os métodos que trarão resultados de relacionamentos como quando um Produto possui uma
Categoria e esta categoria é obtida através de uma outra tabela. Sendo assim é necessário criar:
    * model: Product:
        * function: category() {belongsTo}
    * model: Category
        * function: products() {hasMany}
* Note que um Produto possui apenas uma categoria, ja uma categoria pode estar relacionada a vários produto's'
* Portanto, se for necessário em algum momento, na aplicação, obter qual a categoria de um determinado produto, então,
já teremos um método criado na model que realizará esta função.
 
### Factory
* É como uma fábrica de criação de registros fictícios
* Possui todas as especificações de cada elemento de um modelo para instanciar , em massa, registros para serem salvos
no banco de dados
* Normalmente utiliza-se o faker para criar dados ficticios que serão utilizados para testes
* Apesar de conter todas as especificacoes dos dados a serem criados, a factory não se auto-executa. Para executar uma
factory é necessário utilizar um Seeder

### Seeds
* é responsável por popular o banco de dados com determinados valores
* pode popular o banco com:
    * Dados pré estipulados
    * Dados fictícios
* Quando criamos os registros fictcios, registramos uma factory e determinados a quantidade de dados fictícios que
queremos que sejam criados no banco
* Quando temos algun itens necessários, não fictícios, necessários para o funcionamento correto do sistema, como uma
tabale da configurações, que depende de valores específicos para a aplicação funcionar, então utilizamos o seeder para
criar estes registros. Neste caso não se utiliza uma factory, mas criam-se os dados que serão adicionados no banco
diretamente no corpo do seed.

## Repository
Uma classe intermediária ao Controller e a Model.

## Services
Uma classe que armazena as regras de negócio da aplicação.
* Quando é necessário abstrair uma lógica muito grande. 
* Quando tem um requisito que vá usar muitos outros métodos
* Quando você tiver um método que poderá ser utilizado em diversos controllers, pode centralizar essa lógica em apenas um local: no service

## OAuth2
* é um framework que implementa uma especificação
* Protocolo de comunicação
* Permite acesso de aplicação e API externas
* Toda aplicação e API externão são um cliente
* Todo cliente tem acesso limitado ao método HTTP
* Todo acesso é feito via um token de acesso


* Segundo a especificação do IETF (RFC6749):
    * Resource owner: o usuário
    * Resource Server: a API
    * Authorization server: quase sempre o mesmo que o servidor da API
    * Client: os aplicativos de terceiros


* O tokem de acesso tem seu tempo de vida
* O token de acesso pode ser renovado (refresh token)
* O token de acesso pode ser destruído (revoke)


> https://tools.ietf.org/html/rfc6749


* Segurança com OAuth2
> É obrigatório utilizar HTTPS/SSL
