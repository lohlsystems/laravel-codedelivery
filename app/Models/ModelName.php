<?php

namespace CodeDelivery\Models;

use Illuminate\Database\Eloquent\Model;

/*
 * Use this as a template to create new Models
 */

class ModelName extends Model
{
    protected $fillable = [
        //
    ];
}
